package main

import "fmt"
import "math/big"

func main() {
	var t, n int64
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&n)

		var res = big.NewInt(n)

		for n--; n > 1; n-- {
			var gcd = big.NewInt(n)
			gcd.GCD(nil, nil, gcd, res)
			res.Mul(res, big.NewInt(n))
			res.Div(res, gcd)
		}

		fmt.Println(res.String())
	}
}
