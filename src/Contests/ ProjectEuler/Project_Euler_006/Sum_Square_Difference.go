package main

import "fmt"

func main() {
	const MAX = 10001
	var t, n int

	var sumSquare, squareSum = make([]uint64, MAX), make([]uint64, MAX)

	for i := uint64(1); i < MAX; i++ {
		sumSquare[i] = sumSquare[i-1] + i*i
		squareSum[i] = squareSum[i-1] + i*(i-1)*i + i*i
	}

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&n)
		fmt.Println(squareSum[n] - sumSquare[n])
	}
}
