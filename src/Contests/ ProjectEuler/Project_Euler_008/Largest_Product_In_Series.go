package main

import "fmt"

func main() {
	var t, n, k, product, max, i, j int
	var str string

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&n, &k)
		fmt.Scanln(&str)

		for i, max = 0, 0; i < n-k; i++ {

			for j, product = 0, 1; j < k; j++ {
				product *= int(str[i+j] - 48)
			}

			if product > max {
				max = product
			}
		}

		fmt.Println(max)
	}
}
