package main

import "fmt"

func main() {
	var t, m, n int

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&m)
		fmt.Scanln(&n)

		var arr = make([]int, n)

		for i := 0; i < n; i++ {
			fmt.Scanf("%v", &arr[i])
		}

		var found bool
		for i := 0; i < n-1 && !found; i++ {
			for j := i + 1; j < n && !found; j++ {
				if arr[i]+arr[j] == m {
					found = true
					fmt.Println(i+1, j+1)
				}
			}
		}
	}
}
