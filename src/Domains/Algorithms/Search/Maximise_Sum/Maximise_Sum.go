package main

import "fmt"
import "bufio"
import "os"
import "strconv"

func main() {
	var t, n, m, val, i, max int64
	fmt.Scanln(&t)

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)

	for ; t > 0; t-- {
		scanner.Scan()
		n, _ = strconv.ParseInt(scanner.Text(), 10, 64)
		scanner.Scan()
		m, _ = strconv.ParseInt(scanner.Text(), 10, 64)

		var arr = make(map[int64]int64)
		max = 0

		for i = 1; i <= n; i++ {
			scanner.Scan()
			val, _ = strconv.ParseInt(scanner.Text(), 10, 64)

			var tempArr = make([]int64, 0)

			if val >= m {
				val = val % m
			}
			if val > max {
				max = val
			}

			for k, v := range arr {
				if v == i-1 {
					temp := val + k
					if temp >= m {
						temp -= m
					}

					if arr[temp] != i-1 {
						arr[temp] = i
						if temp > max {
							max = temp
						}
					} else {
						tempArr = append(tempArr, temp)
					}

				}
			}

			arr[val] = i
			for _, v := range tempArr {
				arr[v] = i
				if v > max {
					max = v
				}
			}
		}

		fmt.Println(max)
	}
}
