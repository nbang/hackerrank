package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	const MAX_VAL = 10001
	r := bufio.NewScanner(os.Stdin)
	r.Split(bufio.ScanWords)

	var arr = make([]int, MAX_VAL)
	var n, m, temp int

	r.Scan()
	n, _ = strconv.Atoi(r.Text())

	for r.Scan(); n > 0; n, _ = n-1, r.Scan() {
		temp, _ = strconv.Atoi(r.Text())

		arr[temp]--
	}

	m, _ = strconv.Atoi(r.Text())

	for ; m > 0; m-- {
		r.Scan()
		temp, _ = strconv.Atoi(r.Text())

		arr[temp]++
	}

	for i := 0; i < MAX_VAL; i++ {
		if arr[i] > 0 {
			fmt.Printf("%d ", i)
		}
	}
}
