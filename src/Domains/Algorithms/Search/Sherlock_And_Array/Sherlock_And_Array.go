package main

import "fmt"

func main() {
	var t, n, temp, i int

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&n)

		var arr = make([]int, n+1)
		var found bool

		for i = 1; i <= n; i++ {
			fmt.Scanf("%v", &temp)
			arr[i] = arr[i-1] + temp
		}

		for i = 1; i <= n && !found; i++ {
			if arr[i]+arr[i-1] == arr[n] {
				found = true
			}
		}

		if found {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
