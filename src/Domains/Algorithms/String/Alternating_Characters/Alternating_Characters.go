package main

import "fmt"

func main() {
	var t int
	var str string

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&str)

		var char, deletion = str[0], 0

		for i := 1; i < len(str); i++ {
			if str[i] == char {
				deletion++
			} else {
				char = str[i]
			}
		}

		fmt.Println(deletion)
	}

}
