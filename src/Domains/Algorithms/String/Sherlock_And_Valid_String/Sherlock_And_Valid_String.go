package main

import "fmt"

func main() {
	var str string
	var arr = make([]int, 26)

	fmt.Scanln(&str)

	for i := 0; i < len(str); i++ {
		arr[str[i]-'a']++
	}

	var min, max int

	for i := 0; i < 26; i++ {
		if arr[i] > 0 {
			if arr[i] > max {
				max = arr[i]
			}
			if arr[i] < min || min == 0 {
				min = arr[i]
			}
		}
	}

	if min == max {
		fmt.Println("YES")
	} else {
		if min == 1 {
			var count int
			var valid = true
			for i := 0; i < 26; i++ {
				if arr[i] == 1 {
					count++
				} else if arr[i] < max && arr[i] > 1 {
					valid = false
				}
			}

			if count == 1 && valid {
				fmt.Println("YES")
			} else {
				var dif int
				for i := 0; i < 26; i++ {
					if arr[i] > 0 {
						dif += arr[i] - min
					}
				}
				if dif == 1 {
					fmt.Println("YES")
				} else {
					fmt.Println("NO")
				}
			}
		} else {
			var dif int
			for i := 0; i < 26; i++ {
				if arr[i] > 0 {
					dif += arr[i] - min
				}
			}
			if dif == 1 {
				fmt.Println("YES")
			} else {
				fmt.Println("NO")
			}
		}
	}

}
