package main

import "fmt"

func main() {
	var t int
	var str string

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&str)

		var arr = make([]int, 26)
		var change int

		if len(str)%2 == 0 {
			for i := 0; i < len(str); i++ {
				if i < len(str)/2 {
					arr[str[i]-97]++
				} else {
					arr[str[i]-97]--
				}
			}

			for i := 0; i < 26; i++ {
				if arr[i] > 0 {
					change += arr[i]
				}
			}
			fmt.Println(change)
		} else {
			fmt.Println(-1)
		}
	}

}
