package main

import "fmt"
import "bufio"
import "os"

func main() {
	reader := bufio.NewReader(os.Stdin)
	str, _ := reader.ReadString('\n')

	var arr = make([]bool, 26)
	var pangram = true

	for i := 0; i < len(str); i++ {
		if str[i] >= 'A' && str[i] <= 'Z' {
			arr[str[i]-'A'] = true
		}
		if str[i] >= 'a' && str[i] <= 'z' {
			arr[str[i]-'a'] = true
		}
	}

	for i := 0; i < 26 && pangram; i++ {
		pangram = pangram && arr[i]
	}

	if pangram {
		fmt.Println("pangram")
	} else {
		fmt.Println("not pangram")
	}
}
