package main

import "fmt"
import "bufio"
import "os"

type Gene struct {
	Letter byte
	Remain int
}

func main() {
	var n int
	var char, last byte

	fmt.Scanln(&n)
	var A, C, T, G = n / 4, n / 4, n / 4, n / 4

	reader := bufio.NewReader(os.Stdin)

	var strike = make([]int, n)
	var arr = make([]byte, n)

	for i := 0; i < n; i++ {
		temp, _ := reader.ReadByte()

		switch temp {
		case 'A':
			char = 1
			A--
		case 'C':
			char = 2
			C--
		case 'G':
			char = 3
			G--
		case 'T':
			char = 4
			T--
		}

		arr[i] = char

		if char != last {
			strike[i] = 1
			last = char
		} else {
			strike[i] = strike[i-1] + 1
		}
	}

	var geneData = make([]Gene, 0)
	switch {
	case A < 0:
		geneData = append(geneData, Gene{1, -A})
		fallthrough
	case C < 0:
		geneData = append(geneData, Gene{2, -C})
		fallthrough
	case G < 0:
		geneData = append(geneData, Gene{3, -G})
		fallthrough
	case T < 0:
		geneData = append(geneData, Gene{4, -T})
	}

	if len(geneData) == 0 {
		fmt.Println(0)
	}
	if len(geneData) == 1 {
		//var length = make([]int, n)
		for i := 0; i < n; i++ {
		}

	}

	//sort
	for i := 0; i < len(geneData); i++ {
		for j := i + 1; j < len(geneData); j++ {
			if geneData[i].Remain < geneData[j].Remain {
				geneData[i], geneData[j] = geneData[j], geneData[i]
			}
		}
	}

	fmt.Println(geneData)
	fmt.Println(strike)
	fmt.Println(arr)
}
