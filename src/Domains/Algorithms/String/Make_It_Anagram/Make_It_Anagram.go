package main

import "fmt"

func main() {
	var str1, str2 string
	var arr1, arr2 = make([]int, 26), make([]int, 26)
	var count int

	fmt.Scanln(&str1)
	fmt.Scanln(&str2)
	for i := 0; i < len(str1); i++ {
		arr1[str1[i]-97]++
	}
	for i := 0; i < len(str2); i++ {
		arr2[str2[i]-97]++
	}

	for i := 0; i < 26; i++ {
		if arr1[i] > arr2[i] {
			count += arr1[i] - arr2[i]
		} else {
			count += arr2[i] - arr1[i]
		}
	}

	fmt.Println(count)
}
