package main

import "fmt"

func main() {
	var str string
	var arr = make([]int, 26)
	var count int

	fmt.Scanln(&str)
	for i := 0; i < len(str); i++ {
		arr[str[i]-97]++
	}

	for i := 0; i < 26; i++ {
		if arr[i]%2 == 1 {
			count++
		}
	}

	if count < 2 {
		fmt.Println("YES")
	} else {
		fmt.Println("NO")
	}
}
