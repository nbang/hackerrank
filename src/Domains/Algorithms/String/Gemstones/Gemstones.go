package main

import "fmt"

func main() {
	var n, total int
	var str string
	var res []bool = []bool{true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true}

	fmt.Scanln(&n)

	for ; n > 0; n-- {
		fmt.Scanln(&str)
		var arr = make([]bool, 26)

		for i := 0; i < len(str); i++ {
			arr[str[i]-97] = true
		}

		for i := 0; i < 26; i++ {
			res[i] = res[i] && arr[i]
		}
	}

	for i := 0; i < 26; i++ {
		if res[i] {
			total++
		}
	}

	fmt.Println(total)
}
