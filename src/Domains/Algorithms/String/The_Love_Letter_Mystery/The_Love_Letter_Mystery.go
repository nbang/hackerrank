package main

import "fmt"

func main() {
	var t int
	var str string

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&str)

		var convertion = 0

		for i := 0; i < len(str)/2; i++ {
			temp := int(str[i]) - int(str[len(str)-i-1])
			if temp > 0 {
				convertion += temp
			} else {
				convertion += -temp
			}
		}

		fmt.Println(convertion)
	}

}
