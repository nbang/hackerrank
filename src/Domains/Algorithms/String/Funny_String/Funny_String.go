package main

import "fmt"

func main() {
	var t int
	var str string

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&str)
		var funny = true
		for i := 1; i < len(str)/2+1 && funny; i++ {
			if str[i]-str[i-1] != str[len(str)-i]-str[len(str)-i-1] && str[i]-str[i-1] != str[len(str)-i-1]-str[len(str)-i] {
				funny = false
			}
		}

		if funny {
			fmt.Println("Funny")
		} else {
			fmt.Println("Not Funny")
		}
	}
}
