package main

import "fmt"

func main() {
	var t int
	var str1, str2 string

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var arr = make([]bool, 26)
		var found bool
		fmt.Scanln(&str1)
		fmt.Scanln(&str2)

		for i := 0; i < len(str1); i++ {
			arr[str1[i]-97] = true
		}

		for i := 0; i < len(str2) && !found; i++ {
			if arr[str2[i]-97] {
				found = true
			}
		}

		if found {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}

}
