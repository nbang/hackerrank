package main

import "fmt"

func main() {
	var t int
	var str string

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&str)

		var i, found = 0, false

		for i < len(str)/2 && !found {
			if str[i] == str[len(str)-i-1] {
				i++
			} else {
				found = true
				var k = i + 1

				for k < len(str)/2 && str[k] == str[len(str)-k] {
					k++
				}
				if k != len(str)/2 {
					i = len(str) - i - 1
				}
			}
		}
		if !found {
			fmt.Println(-1)
		} else {
			fmt.Println(i)
		}
	}

}
