package main

import "fmt"

func main() {
	var str1, str2 string

	fmt.Scanln(&str1)
	fmt.Scanln(&str2)

	var arr1, arr2 = make([]int, len(str1)+1), make([]int, len(str1)+1)

	for i := 0; i < len(str2); i++ {
		for j := 0; j < len(str1); j++ {
			if str2[i] == str1[j] {
				arr2[j+1] = arr1[j] + 1
			} else if arr1[j+1] > arr2[j] {
				arr2[j+1] = arr1[j+1]
			} else {
				arr2[j+1] = arr2[j]
			}
		}

		arr1, arr2 = arr2, arr1
	}

	fmt.Println(arr1[len(str1)])
}
