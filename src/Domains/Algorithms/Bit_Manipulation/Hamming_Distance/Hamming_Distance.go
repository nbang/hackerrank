package main

import "fmt"
import "bufio"
import "os"
import "strconv"

func main() {
	var n, m, i, l1, r1, l2, r2, len, count int
	var c byte

	fmt.Scanln(&n)

	scanner := bufio.NewScanner(os.Stdin)
	scanner.Split(bufio.ScanWords)

	var arr = make([]byte, n+1)

	_, arr = scanner.Scan(), scanner.Bytes()
	arr = append([]byte{'_'}, arr...)

	scanner.Scan()
	m, _ = strconv.Atoi(scanner.Text())

	for ; m > 0; m-- {

		scanner.Scan()
		switch scanner.Bytes()[0] {
		case 'C':
			{
				scanner.Scan()
				l1, _ = strconv.Atoi(scanner.Text())
				scanner.Scan()
				r1, _ = strconv.Atoi(scanner.Text())
				scanner.Scan()
				c = scanner.Bytes()[0]
				//fmt.Println(l1, r1, c)
				for i = l1; i <= r1; i++ {
					arr[i] = c
				}
			}
		case 'S':
			{
				scanner.Scan()
				l1, _ = strconv.Atoi(scanner.Text())
				scanner.Scan()
				r1, _ = strconv.Atoi(scanner.Text())
				scanner.Scan()
				l2, _ = strconv.Atoi(scanner.Text())
				scanner.Scan()
				r2, _ = strconv.Atoi(scanner.Text())
				//fmt.Println(l1, r1, l2, r2)

				var temp = append([]byte{}, arr[:l1]...)
				temp = append(temp, arr[l2:r2+1]...)
				temp = append(temp, arr[r1+1:l2]...)
				temp = append(temp, arr[l1:r1+1]...)
				temp = append(temp, arr[r2+1:]...)

				arr = temp
			}
		case 'R':
			{
				scanner.Scan()
				l1, _ = strconv.Atoi(scanner.Text())
				scanner.Scan()
				r1, _ = strconv.Atoi(scanner.Text())
				//fmt.Println(l1, r1)

				for i = l1; i <= (r1+l1)/2; i++ {
					arr[i], arr[r1+l1-i] = arr[r1+l1-i], arr[i]
				}
			}
		case 'W':
			{
				scanner.Scan()
				l1, _ = strconv.Atoi(scanner.Text())
				scanner.Scan()
				r1, _ = strconv.Atoi(scanner.Text())
				//fmt.Println(l1, r1)
				fmt.Printf("%s\n", string(arr[l1:r1+1]))
			}
		case 'H':
			{
				scanner.Scan()
				l1, _ = strconv.Atoi(scanner.Text())
				scanner.Scan()
				l2, _ = strconv.Atoi(scanner.Text())
				scanner.Scan()
				len, _ = strconv.Atoi(scanner.Text())
				//fmt.Println(l1, l2, len)
				if l1 == l2 {
					fmt.Println(0)
				} else {
					for i, count = 0, 0; i < len; i++ {
						if arr[i+l1] != arr[i+l2] {
							count++
						}
					}
					fmt.Println(count)
				}
			}
		}
		//fmt.Printf("Res: %s\n", arr)
	}
}
