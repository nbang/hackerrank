package main

import "fmt"

func main() {
	const MAX = uint64(1000000007)
	var t, n, sum, i uint64

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&n)
		var arr = make([]uint64, n)

		for sum, i = 0, 0; i < n; i++ {
			fmt.Scanf("%v", &arr[i])
			sum = sum | arr[i]
		}

		for n--; n > 0; n-- {
			sum = sum << 1
			if sum >= MAX {
				sum = sum % MAX
			}
		}

		fmt.Println(sum)
	}
}
