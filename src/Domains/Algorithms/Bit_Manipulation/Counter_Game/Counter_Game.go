package main

import "fmt"

func main() {
	var t, n, step, i uint64
	var arr = make([]uint64, 64)

	arr[0] = 1
	for i = 1; i < 64; i++ {
		arr[i] = arr[i-1] << 1
	}

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&n)

		for step = 0; n > 1; step++ {
			for i = 1; i < 64 && arr[i] < n; i++ {
			}

			if i == 64 || arr[i] > n {
				n -= arr[i-1]
			} else {
				n = n >> 1
			}
		}

		if step%2 == 0 {
			fmt.Println("Richard")
		} else {
			fmt.Println("Louise")
		}
	}
}
