package main

import "fmt"

func main() {
	var l, r, max int

	fmt.Scanln(&l)
	fmt.Scanln(&r)

	for i := l; i <= r; i++ {
		for j := l; j <= r; j++ {
			if i^j > max {
				max = i ^ j
			}
		}
	}

	fmt.Println(max)
}
