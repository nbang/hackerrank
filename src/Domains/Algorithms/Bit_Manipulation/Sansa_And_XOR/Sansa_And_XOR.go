package main

import "fmt"

func main() {
	var t, n, res int

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		res = 0
		fmt.Scanln(&n)
		var arr = make([]int, n)

		for i := 0; i < n; i++ {
			fmt.Scanf("%v", &arr[i])

			if (n+i)%2 == 1 {
				res = res ^ arr[i]
			}
		}

		fmt.Println(res)
	}
}
