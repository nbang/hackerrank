package main

import "fmt"

func main() {
	var t, temp, l, r uint64

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&l, &r)

		switch {
		case (l%8 == 0 || l%8 == 7) && (r%8 == 0 || r%8 == 1):
			temp = r
		case (l%8 == 0 || l%8 == 7) && (r%8 == 2 || r%8 == 3):
			temp = 2
		case (l%8 == 0 || l%8 == 7) && (r%8 == 4 || r%8 == 5):
			temp = r ^ 2
		case (l%8 == 0 || l%8 == 7) && (r%8 == 6 || r%8 == 7):
			temp = 0

		case (l%8 == 1 || l%8 == 6) && (r%8 == 0 || r%8 == 1):
			temp = r ^ l ^ 1
		case (l%8 == 1 || l%8 == 6) && (r%8 == 2 || r%8 == 3):
			temp = l ^ 3
		case (l%8 == 1 || l%8 == 6) && (r%8 == 4 || r%8 == 5):
			temp = r ^ l ^ 3
		case (l%8 == 1 || l%8 == 6) && (r%8 == 6 || r%8 == 7):
			temp = l ^ 1

		case (l%8 == 2 || l%8 == 5) && (r%8 == 0 || r%8 == 1):
			temp = r ^ l ^ 3
		case (l%8 == 2 || l%8 == 5) && (r%8 == 2 || r%8 == 3):
			temp = l ^ 1
		case (l%8 == 2 || l%8 == 5) && (r%8 == 4 || r%8 == 5):
			temp = r ^ l ^ 1
		case (l%8 == 2 || l%8 == 5) && (r%8 == 6 || r%8 == 7):
			temp = l ^ 3

		case (l%8 == 3 || l%8 == 4) && (r%8 == 0 || r%8 == 1):
			temp = r ^ 2
		case (l%8 == 3 || l%8 == 4) && (r%8 == 2 || r%8 == 3):
			temp = 0
		case (l%8 == 3 || l%8 == 4) && (r%8 == 4 || r%8 == 5):
			temp = r
		case (l%8 == 3 || l%8 == 4) && (r%8 == 6 || r%8 == 7):
			temp = 2
		}

		fmt.Println(temp)
	}
}
