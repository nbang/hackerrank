package main

import "fmt"

func main() {
	var n, k int
	var str string

	fmt.Scanln(&n, &k)
	fmt.Scanln(&str)

	var arr = make([]byte, n)

	for i := 0; i < n; i++ {
		var xor byte
		if i == 0 {
			arr[i] = str[i] - 48
		} else if i < k {
			arr[i] = (str[i] - 48) ^ (str[i-1] - 48)
		} else {
			for j := i - 1; i-j < k; j-- {
				xor = xor ^ arr[j]
			}
			arr[i] = xor ^ (str[i] - 48)
		}

		fmt.Printf("%v", arr[i])
	}
}
