package main

import "fmt"

func main() {
	var t, n int
	const MAX = 1<<32 - 1

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&n)
		fmt.Println(MAX - n)
	}
}
