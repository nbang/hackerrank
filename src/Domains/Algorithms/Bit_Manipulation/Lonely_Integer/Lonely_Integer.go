package main

import "fmt"

func main() {
	var n, val int
	fmt.Scanln(&n)

	var arr = make([]bool, 101)

	for ; n > 0; n-- {
		fmt.Scanf("%v", &val)
		arr[val] = !arr[val]
	}

	for i := 0; i <= 100; i++ {
		if arr[i] {
			fmt.Println(i)
		}
	}
}
