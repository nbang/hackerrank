package main

import "fmt"
import "time"

func main() {
	var str string
	fmt.Scanln(&str)
	var t, _ = time.Parse("03:04:05PM", str)
	fmt.Println(t.Format("15:04:05"))
}
