package main

import "fmt"

func main() {
	var n int
	var cur, positive, negative float32
	fmt.Scanln(&n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%v", &cur)
		if cur > 0 {
			positive += 1 / float32(n)
		}
		if cur < 0 {
			negative += 1 / float32(n)
		}
	}
	fmt.Printf("%v\n%v\n%v", positive, negative, 1-positive-negative)
}
