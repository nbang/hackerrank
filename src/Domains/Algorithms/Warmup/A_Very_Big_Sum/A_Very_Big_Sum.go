package main

import "fmt"

func main() {
	var n, cur, res uint64
	fmt.Scanln(&n)
	for ; n > 0; n-- {
		fmt.Scanf("%v", &cur)
		res += cur
	}
	fmt.Println(res)
}
