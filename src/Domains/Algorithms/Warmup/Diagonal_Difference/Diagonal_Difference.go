package main

import "fmt"

func main() {
	var n, cur, diff int
	fmt.Scanln(&n)
	for i := 0; i < n; i++ {
		for j := 0; j < n; j++ {
			fmt.Scanf("%v", &cur)
			if i == j {
				diff += cur
			}
			if i+j == n-1 {
				diff -= cur
			}
		}
	}
	if diff < 0 {
		diff = 0 - diff
	}
	fmt.Println(diff)
}
