package main

import "fmt"

func main() {
	var n, cur, res int
	fmt.Scanln(&n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%v", &cur)
		res += cur
	}
	fmt.Println(res)
}
