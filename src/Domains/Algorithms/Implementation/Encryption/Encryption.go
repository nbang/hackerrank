package main

import "fmt"
import "math"

func main() {
	var str string
	fmt.Scanln(&str)

	var step = int(math.Sqrt(float64(len(str)-1))) + 1

	for i := 0; i < step; i++ {
		for j := i; j < len(str); j += step {
			fmt.Printf("%s", string(str[j]))
		}
		fmt.Print(" ")
	}

}
