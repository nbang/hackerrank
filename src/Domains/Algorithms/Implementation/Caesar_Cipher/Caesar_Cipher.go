package main

import "fmt"

func main() {
	var n, k, temp byte
	var str string
	fmt.Scanln(&n)
	fmt.Scanln(&str)
	fmt.Scanln(&k)

	for i := 0; i < len(str); i++ {
		if str[i] >= 'A' && str[i] <= 'Z' {
			for temp = str[i] + k; temp > 'Z'; temp -= 26 {
			}
			fmt.Printf("%s", string(temp))
		} else if str[i] >= 'a' && str[i] <= 'z' {
			for temp = str[i] + k; temp > 'z'; temp -= 26 {
			}
			fmt.Printf("%s", string(temp))
		} else {
			fmt.Printf("%s", string(str[i]))
		}

	}

}
