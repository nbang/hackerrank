package main

import "fmt"

func main() {
	var n, m, r int
	fmt.Scanln(&n, &m, &r)

	var arr = make([][]int, n)
	for i := 0; i < n; i++ {
		arr[i] = make([]int, m)
		for j := 0; j < m; j++ {
			fmt.Scanf("%v", &arr[i][j])
		}
	}

	for i := 0; i < n/2 && i < m/2; i++ {
		var temp []int

		//append
		for k := 0; k < m-i*2; k++ {
			temp = append(temp, arr[i][i+k])
		}
		for k := 1; k < n-i*2; k++ {
			temp = append(temp, arr[i+k][m-i-1])
		}
		for k := 1; k < m-i*2; k++ {
			temp = append(temp, arr[n-i-1][m-i-k-1])
		}
		for k := 1; k < n-i*2-1; k++ {
			temp = append(temp, arr[n-i-k-1][i])
		}
		for tempR := r % ((m+n)*2 - i*8 - 4); tempR > 0; tempR-- {
			temp = append(temp[1:], temp[0])
		}

		for k := 0; k < m-i*2; k++ {
			arr[i][i+k] = temp[k]
		}
		for k := 1; k < n-i*2; k++ {
			arr[i+k][m-i-1] = temp[k+m-i*2-1]
		}
		for k := 1; k < m-i*2; k++ {
			arr[n-i-1][m-i-k-1] = temp[k+m+n-i*4-2]
		}
		for k := 1; k < n-i*2-1; k++ {
			arr[n-i-k-1][i] = temp[k+m*2+n-i*6-3]
		}
	}

	for i := 0; i < n; i++ {
		for j := 0; j < m; j++ {
			fmt.Printf("%v ", arr[i][j])
		}
		fmt.Println()
	}
}
