package main

import "fmt"
import "math/big"

func main() {
	var n int
	fmt.Scanln(&n)

	var res = big.NewInt(1)

	if n > 1 {
		for i := 2; i <= n; i++ {
			res.Mul(res, big.NewInt(int64(i)))
		}
		fmt.Println(res.String())
	} else {
		fmt.Println(1)
	}
}
