package main

import "fmt"

func main() {
	var t, n int

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&n)

		var arr = make([]int, n+1)

		for i := 1; i <= n; i++ {
			fmt.Scan(&arr[i])
		}
		var bribe int
		for i := n; i > 1 && bribe >= 0; i-- {
			if arr[i] != i {
				if arr[i-1] != i {
					if arr[i-2] != i {
						bribe = -1
					} else {
						arr[i-2], arr[i-1], arr[i] = arr[i-1], arr[i], arr[i-2]
						bribe += 2
					}
				} else {
					arr[i], arr[i-1] = arr[i-1], arr[i]
					bribe++
				}
			}
		}

		if bribe >= 0 {
			fmt.Println(bribe)
		} else {
			fmt.Println("Too chaotic")
		}
	}
}
