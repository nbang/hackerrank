package main

import "fmt"

func main() {
	// List can be found at https://oeis.org/A053816
	var arr = []int{
		1, 9, 45, 55, 99, 297, 703, 999, 2223, 2728, 4950, 5050, 7272, 7777, 9999, 17344, 22222, 77778, 82656, 95121, 99999,
	}
	var p, q int

	fmt.Scanln(&p)
	fmt.Scanln(&q)

	var found bool
	for i := 0; i < len(arr); i++ {
		if arr[i] >= p && arr[i] <= q {
			fmt.Printf("%v ", arr[i])
			found = true
		}
	}

	if !found {
		fmt.Println("INVALID RANGE")
	}
}
