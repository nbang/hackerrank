package main

import "fmt"

func main() {
	var t, b, w, x, y, z int

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&b, &w)
		fmt.Scanln(&x, &y, &z)

		if x == y {
			fmt.Println(x * (b + w))
		} else {
			if x > y {
				x, y = y, x
				b, w = w, b
			}

			if x+z < y {
				fmt.Println(x*(b+w) + z*w)
			} else {
				fmt.Println(x*b + y*w)
			}
		}
	}
}
