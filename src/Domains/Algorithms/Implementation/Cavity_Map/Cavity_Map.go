package main

import "fmt"

func main() {
	var t int
	fmt.Scanln(&t)

	var arr = make([]string, t)

	for i := 0; i < t; i++ {
		fmt.Scanln(&arr[i])
	}

	for i := 0; i < t; i++ {
		for j := 0; j < t; j++ {
			if i > 0 && i < t-1 && j > 0 && j < t-1 &&
				arr[i][j] > arr[i-1][j] && arr[i][j] > arr[i+1][j] && arr[i][j] > arr[i][j-1] && arr[i][j] > arr[i][j+1] {
				fmt.Printf("X")
			} else {
				fmt.Printf("%s", string(arr[i][j]))

			}
		}
		fmt.Println()
	}
}
