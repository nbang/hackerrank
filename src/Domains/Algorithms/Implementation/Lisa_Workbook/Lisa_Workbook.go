package main

import "fmt"

func main() {
	var k, n, chapter int
	fmt.Scanln(&n, &k)
	page, special := 0, 0
	for ; n > 0; n-- {
		fmt.Scanf("%v", &chapter)
		for i := 1; chapter > 0; i, chapter = i+k, chapter - k {
			page++
			if i <= page && i+k > page && i+chapter > page {
				special++
			}
		}
	}
	fmt.Println(special)
}
