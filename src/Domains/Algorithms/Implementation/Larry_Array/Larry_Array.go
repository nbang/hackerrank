package main

import "fmt"

func main() {
	var t, n int

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&n)

		var arr = make([]int, n)
		for i := 0; i < n; i++ {
			fmt.Scanf("%v", &arr[i])
		}

		for i := 0; i < n-2; i++ {
			if arr[i] > i+1 {
				var index int
				for index = i; arr[index] > i+1; index++ {
				}

				for index > i {
					if index > i+1 {
						arr[index-2], arr[index-1], arr[index] = arr[index], arr[index-2], arr[index-1]
						index -= 2
					} else {
						arr[index-1], arr[index], arr[index+1] = arr[index], arr[index+1], arr[index-1]
						index -= 1
					}
				}
			}
		}

		if arr[len(arr)-1] == len(arr)-1 {
			fmt.Println("NO")
		} else {
			fmt.Println("YES")
		}
	}
}
