package main

import "fmt"
import "sort"

func main() {
	var t, a, b, n int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanf("%d\n%d\n%d\n", &n, &a, &b)

		var m = make(map[int]int)
		var keys []int

		m[a] = a
		m[b] = b

		for ; n > 2; n-- {
			var temp = make(map[int]int)
			for k := range m {
				temp[k+a] = a
				temp[k+b] = b
			}
			m = temp
		}

		for k := range m {
			keys = append(keys, k)
		}

		sort.Ints(keys)

		for i := 0; i < len(keys); i++ {
			fmt.Printf("%d ", keys[i])
		}
		fmt.Println()

	}
}
