package main

import "fmt"

func main() {
	var n, m int
	var str string
	fmt.Scanln(&n, &m)

	var arr = make([][]bool, n)
	var res = make([]int, m)

	for i := 0; i < n; i++ {
		fmt.Scanln(&str)
		arr[i] = make([]bool, len(str))
		for j := 0; j < len(str); j++ {
			if str[i] > '0' {
				arr[i][j] = false
			} else {
				arr[i][j] = true
			}
		}
	}

	for i := 0; i < n-1; i++ {
		for j := i + 1; j < n; j++ {
			var count = 0
			for k := 0; k < m; k++ {
				if arr[i][k] || arr[j][k] {
					count++
				}
			}
			res[count]++
		}
	}

	for m--; m > 0 && res[m] == 0; m-- {
	}
	fmt.Printf("%d\n%d", m, res[m])

}
