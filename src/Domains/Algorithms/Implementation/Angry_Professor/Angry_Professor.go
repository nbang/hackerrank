package main

import "fmt"

func main() {
	var t, n, k, time int
	fmt.Scanln(&t)
	for i := 0; i < t; i++ {
		fmt.Scanf("%v%v", &n, &k)
		var count int
		for j := 0; j < n; j++ {
			fmt.Scanf("%v", &time)
			if time <= 0 {
				count++
			}
		}
		if count < k {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
