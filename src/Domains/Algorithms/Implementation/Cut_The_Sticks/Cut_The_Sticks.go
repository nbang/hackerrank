package main

import "fmt"

func main() {
	var n, k, temp int
	fmt.Scanln(&n, &k)
	arr := make([]int, 1000)
	for i := 0; i < n; i++ {
		fmt.Scanf("%v", &temp)
		arr[temp]++
	}

	for i := 0; i < 1000 && n > 0; i++ {
		if arr[i] > 0 {
			fmt.Println(n)
			n -= arr[i]
		}
	}
}
