package main

import "fmt"

var textNumbers = []string{
	"o' clock", "one", "two", "three", "four",
	"five", "six", "seven", "eight", "nine",
	"ten", "eleven", "twelve", "thirteen", "fourteen",
	"quarter", "sixteen", "seventeen", "eighteen", "nineteen",
	"twenty", "twenty one", "twenty two", "twenty three", "twenty four",
	"twenty five", "twenty six", "twenty seven", "twenty eight", "twenty nine",
	"half"}

func main() {
	var h, m int
	fmt.Scanln(&h)
	fmt.Scanln(&m)

	switch {
	case m == 0:
		fmt.Printf("%v %v", textNumbers[h], textNumbers[m])
	case m == 1:
		fmt.Printf("%v minute past %v", textNumbers[m], textNumbers[h])
	case m > 1 && m < 15:
		fmt.Printf("%v minutes past %v", textNumbers[m], textNumbers[h])
	case m == 15 || m == 30:
		fmt.Printf("%v past %v", textNumbers[m], textNumbers[h])
	case m > 16 && m < 30:
		fmt.Printf("%v minutes past %v", textNumbers[m], textNumbers[h])
	case m > 30 && m < 45:
		fmt.Printf("%v minutes to %v", textNumbers[60-m], textNumbers[h+1])
	case m == 45:
		fmt.Printf("%v to %v", textNumbers[60-m], textNumbers[h+1])
	case m > 45 && m < 59:
		fmt.Printf("%v minutes to %v", textNumbers[60-m], textNumbers[h+1])
	case m == 59:
		fmt.Printf("%v minute to %v", textNumbers[60-m], textNumbers[h+1])
	}
}
