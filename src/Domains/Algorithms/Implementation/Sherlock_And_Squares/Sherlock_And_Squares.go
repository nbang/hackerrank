package main

import "fmt"
import "math"

func main() {
	var t, a, b int
	fmt.Scanln(&t)
	for ; t > 0; t-- {
		fmt.Scanf("%v%v", &a, &b)

		fmt.Println(int(math.Sqrt(float64(b))) - int(math.Sqrt(float64(a-1))))
	}
}
