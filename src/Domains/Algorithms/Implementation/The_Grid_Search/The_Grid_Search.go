package main

import "fmt"

func main() {
	var t, a, b, x, y int
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&a, &b)
		var arr = make([]string, a)
		for i := 0; i < a; i++ {
			fmt.Scanln(&arr[i])
		}

		fmt.Scanln(&x, &y)
		var pattern = make([]string, x)
		for i := 0; i < x; i++ {
			fmt.Scanln(&pattern[i])
		}

		var matched = false
		for i := 0; i <= a-x && !matched; i++ {
			for j := 0; j <= b-y && !matched; j++ {
				if arr[i][j] == pattern[0][0] {
					var valid = true
					for k := 0; k < x && valid; k++ {
						for l := 0; l < y && valid; l++ {
							if arr[k+i][l+j] != pattern[k][l] {
								valid = false
							}
						}
					}
					if valid {
						matched = true
					}
				}
			}
		}
		if matched {
			fmt.Println("YES")
		} else {
			fmt.Println("NO")
		}
	}
}
