package main

import "fmt"

func main() {
	var t, n int
	res := make([]uint64, 100)
	res[0] = 1
	for i := 0; i < 60; i++ {
		if i%2 == 1 {
			res[i+1] = res[i] + 1
		} else {
			res[i+1] = res[i] * 2
		}
	}

	fmt.Scanln(&t)
	for ; t > 0; t-- {
		fmt.Scanf("%v", &n)

		fmt.Println(res[n])
	}
}
