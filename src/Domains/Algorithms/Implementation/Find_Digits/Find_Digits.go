package main

import "fmt"

func main() {
	var t, n int
	var str string
	fmt.Scanln(&t)
	for ; t > 0; t-- {
		fmt.Scanln(&str)
		fmt.Sscan(str, n)
		count := 0
		for _, v := range str {
			if v > 48 && n/int(v-48) == 0 {
				count++
			}
		}
		fmt.Println(count)
	}
}
