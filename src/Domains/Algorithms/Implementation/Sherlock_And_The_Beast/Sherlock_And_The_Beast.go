package main

import "fmt"
import "bytes"

func beast(n int, prefix bytes.Buffer) string {
	if n == 0 {
		return prefix.String()
	}
	if n == 1 || n == 2 || n == 4 {
		return "-1"
	}
	if n == 5 || n == 10 {
		prefix.WriteString("33333")
		return beast(n-5, prefix)
	}
	for ; n > 13; n = n - 3 {
		prefix.WriteString("555")
	}

	prefix.WriteString("555")
	return beast(n-3, prefix)
}
func main() {
	var n, t int
	var buf bytes.Buffer
	fmt.Scanln(&t)
	for ; t > 0; t-- {
		fmt.Scanf("%v", &n)
		fmt.Println(beast(n, buf))
	}
}
