package main

import "fmt"

func main() {
	var t, n, a, b int
	fmt.Scanln(&n, &t)
	arr := make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%v", &arr[i])
	}

	for ; t > 0; t-- {
		fmt.Scanf("%v%v\n", &a, &b)
		min := 3
		for i := a; i <= b && min > 1; i++ {
			if arr[i] < min {
				min = arr[i]
			}
		}
		fmt.Println(min)
	}
}
