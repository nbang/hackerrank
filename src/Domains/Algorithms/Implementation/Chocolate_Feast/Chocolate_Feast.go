package main

import "fmt"

func main() {
	var t, n, c, m int
	fmt.Scanln(&t)
	for ; t > 0; t-- {
		fmt.Scanln(&n, &c, &m)
		total := n / c
		for temp := total; temp >= m; {
			total++
			temp = temp - m + 1
		}
		fmt.Println(total)
	}
}
