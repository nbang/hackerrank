package main

import "fmt"

func main() {
	var n, temp, i, j, shift int

	fmt.Scanln(&n)
	var arr = make([]int, n)

	for i = 0; i < n; i++ {
		fmt.Scanf("%v", &arr[i])
	}

	for i = 1; i < n; i++ {
		for j, temp = i, arr[i]; j > 0 && arr[j-1] > temp; j-- {
			arr[j] = arr[j-1]
			shift++
		}

		arr[j] = temp
	}

	fmt.Println(shift)
}
