package main

import "fmt"

func printArr(arr []int) {
	var str = fmt.Sprint(arr)
	fmt.Println(str[1 : len(str)-1])
}

func main() {
	var n, val int

	fmt.Scanln(&n)
	fmt.Scanf("%v", &val)

	var left, right = []int{}, []int{val}

	for i := 1; i < n; i++ {
		fmt.Scanf("%v", &val)
		if val < right[0] {
			left = append(left, val)
		} else {
			right = append(right, val)
		}
	}

	printArr(append(left, right...))
}
