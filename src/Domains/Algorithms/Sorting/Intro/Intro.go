package main

import "fmt"

func main() {
	var v, n, temp int
	var found bool
	fmt.Scanln(&v)
	fmt.Scanln(&n)
	for i := 0; i < n && !found; i++ {
		fmt.Scanf("%v", &temp)
		if temp == v {
			found = true
			fmt.Println(i)
		}
	}
}
