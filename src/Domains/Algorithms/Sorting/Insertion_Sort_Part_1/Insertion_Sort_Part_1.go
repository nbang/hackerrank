package main

import "fmt"

func printArr(arr []int) {
	var str = fmt.Sprint(arr)
	fmt.Println(str[1 : len(str)-1])
}

func main() {
	var n, temp, i int

	fmt.Scanln(&n)
	var arr = make([]int, n)

	for i = 0; i < n; i++ {
		fmt.Scanf("%v", &arr[i])
	}

	for i, temp = n-1, arr[n-1]; i > 0 && arr[i-1] > temp; i-- {
		arr[i] = arr[i-1]
		printArr(arr)
	}

	arr[i] = temp
	printArr(arr)
}
