package main

import "fmt"

func printArr(arr []int) {
	var str = fmt.Sprint(arr)
	fmt.Println(str[1 : len(str)-1])
}

func main() {
	var n, temp, i, j int

	fmt.Scanln(&n)
	var arr = make([]int, n)

	for i = 0; i < n; i++ {
		fmt.Scanf("%v", &arr[i])
	}

	for i = 1; i < n; i++ {
		for j, temp = i, arr[i]; j > 0 && arr[j-1] > temp; j-- {
			arr[j] = arr[j-1]
		}

		arr[j] = temp
		printArr(arr)
	}
}
