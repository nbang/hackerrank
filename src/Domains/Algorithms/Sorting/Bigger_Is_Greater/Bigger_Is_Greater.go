package main

import "fmt"
import "bufio"
import "os"

func main() {
	var t, i, j int
	var str []byte

	fmt.Scanln(&t)

	var rd = bufio.NewReader(os.Stdin)

	for ; t > 0; t-- {
		str, _, _ = rd.ReadLine()

		for i = len(str) - 1; i > 0 && str[i] <= str[i-1]; i-- {
		}

		if i == 0 {
			fmt.Println("no answer")
		} else {
			for j, i = i, i-1; j < len(str) && str[i] < str[j]; j++ {
			}
			str[i], str[j-1] = str[j-1], str[i]

			for j = i + 1; j <= (len(str)+i)/2; j++ {
				str[j], str[len(str)-j+i] = str[len(str)-j+i], str[j]
			}

			fmt.Println(string(str))
		}
	}
}
