package main

import "fmt"

func printArr() {
	var str = fmt.Sprint(arr)
	fmt.Println(str[1 : len(str)-1])
}

func quickSort(arr []int) {
	if len(arr) > 1 {
		var j, pivot = 0, arr[len(arr)-1]

		for i := 0; i < len(arr)-1; i++ {
			if arr[i] < pivot {
				if j < i {
					arr[i], arr[j] = arr[j], arr[i]
				}
				j++
			}
		}

		if j < len(arr)-1 {
			arr[j], arr[len(arr)-1] = arr[len(arr)-1], arr[j]
		}

		printArr()

		quickSort(arr[:j])
		quickSort(arr[j+1:])
	}
}

var arr []int

func main() {
	var n int

	fmt.Scanln(&n)

	arr = make([]int, n)

	for i := 0; i < n; i++ {
		fmt.Scanf("%v", &arr[i])
	}

	quickSort(arr)
}
