package main

import "fmt"

func printArr(arr []int) {
	var str = fmt.Sprint(arr)
	fmt.Println(str[1 : len(str)-1])
}

func quickSort(arr []int) []int {
	var left, right = []int{}, []int{}

	if len(arr) > 0 {
		var pivot = arr[0]
		for i := 1; i < len(arr); i++ {
			if arr[i] < pivot {
				left = append(left, arr[i])
			} else {
				right = append(right, arr[i])
			}
		}

		left = quickSort(left)
		right = quickSort(right)

		arr = append(append(left, pivot), right...)
		if len(arr) > 1 {
			printArr(arr)
		}
	}

	return arr
}

func main() {
	var n int

	fmt.Scanln(&n)

	var arr = make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%v", &arr[i])
	}

	quickSort(arr)
}
