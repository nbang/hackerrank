package main

import "fmt"
import "math/big"

func main() {
	var n, a, b int64
	fmt.Scanln(&a, &b, &n)

	var bigA, bigB = big.NewInt(a), big.NewInt(b)

	for n--; n > 1; n-- {
		var temp = big.NewInt(0)
		temp.Mul(bigB, bigB)
		temp.Add(temp, bigA)
		bigA, bigB = bigB, temp
	}
	fmt.Println(bigB)

}
