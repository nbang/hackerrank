package main

import "fmt"

func main() {
	var n, m, a, b, cut int
	fmt.Scanln(&n, &m)

	var parents, childCounts = make([]int, n+1), make([]int, n+1)

	for i := 0; i < m; i++ {
		fmt.Scanln(&a, &b)
		parents[a] = b
		childCounts[b]++
		for temp := b; parents[temp] > 0; temp = parents[temp] {
			childCounts[parents[temp]]++
		}
	}

	for i := 1; i <= n; i++ {
		if parents[i] > 0 && childCounts[i]%2 == 1 {
			cut++
			for temp := i; parents[temp] > 0; temp = parents[temp] {
				childCounts[parents[temp]] -= childCounts[i] + 1
			}
			parents[i] = 0
		}
	}

	fmt.Println(cut)
}
