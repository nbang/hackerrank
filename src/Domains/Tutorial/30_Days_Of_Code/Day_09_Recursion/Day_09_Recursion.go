package main

import "fmt"

func factorial(n uint64) uint64 {
	if n < 2 {
		return 1
	}

	return n * factorial(n-1)
}

func main() {
	var n uint64

	fmt.Scanln(&n)
	fmt.Println(factorial(n))
}
