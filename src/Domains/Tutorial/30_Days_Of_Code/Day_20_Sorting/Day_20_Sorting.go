package main

import "fmt"

func main() {
	var n, swap int

	fmt.Scanln(&n)
	var arr = make([]int, n)

	for i := 0; i < n; i++ {
		fmt.Scan(&arr[i])
	}

	for i := 0; i < n; i++ {
		var local int
		for j := 0; j < n-1; j++ {
			if arr[j] > arr[j+1] {
				arr[j], arr[j+1] = arr[j+1], arr[j]
				local++
			}
		}
		if local == 0 {
			break
		} else {
			swap += local
		}
	}

	fmt.Printf("Array is sorted in %d swaps.\n", swap)
	fmt.Printf("First Element: %d\n", arr[0])
	fmt.Printf("Last Element: %d\n", arr[n-1])
}
