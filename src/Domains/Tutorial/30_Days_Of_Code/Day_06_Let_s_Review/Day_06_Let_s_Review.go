package main

import "fmt"

func main() {
	var t int
	var str string
	fmt.Scanln(&t)

	for ; t > 0; t-- {
		fmt.Scanln(&str)
		for i := 0; i < len(str); i += 2 {
			fmt.Printf("%c", str[i])
		}
		fmt.Printf(" ")
		for i := 1; i < len(str); i += 2 {
			fmt.Printf("%c", str[i])
		}
		fmt.Println()
	}
}
