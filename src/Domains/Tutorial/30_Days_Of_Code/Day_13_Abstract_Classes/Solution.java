import java.util.*;

abstract class Book {
	String title;
	String author;

	Book(String t, String a) {
		title = t;
		author = a;
	}

	abstract void display();

}// Write MyBook Class

class MyBook extends Book {
	private int price;

	public MyBook(String t, String a, int p) {
		super(t, a);
		price = p;
	}

	public void display() {
		System.out.printf("Title: %s\n", title);
		System.out.printf("Author: %s\n", author);
		System.out.printf("Price: %d\n", price);
	}
}

public class Solution {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String title = sc.nextLine();
		String author = sc.nextLine();
		int price = sc.nextInt();
		Book new_novel = new MyBook(title, author, price);
		new_novel.display();

	}
}