package main

import "fmt"

func main() {
	var n int
	fmt.Scanln(&n)
	var arr = make([]int, n)
	for i := 0; i < n; i++ {
		fmt.Scanf("%v", &arr[i])
	}

	for i := n - 1; i >= 0; i-- {
		if i > 0 {
			fmt.Printf("%v ", arr[i])
		} else {
			fmt.Printf("%v", arr[i])
		}
	}
}
