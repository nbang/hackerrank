package main

import "fmt"

func main() {
	var max = -100
	var arr [6][6]int

	for i := 0; i < 6; i++ {
		for j := 0; j < 6; j++ {
			fmt.Scanf("%v", &arr[i][j])
		}
	}

	for i := 0; i < 4; i++ {
		for j := 0; j < 4; j++ {
			var sum = arr[i][j] + arr[i][j+1] + arr[i][j+2] + arr[i+1][j+1] + arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2]

			if sum > max {
				max = sum
			}
		}
	}

	fmt.Println(max)
}
