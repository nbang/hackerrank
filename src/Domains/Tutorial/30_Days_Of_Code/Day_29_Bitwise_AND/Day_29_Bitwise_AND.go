package main

import "fmt"

func main() {
	var t, n, k int

	fmt.Scanln(&t)

	for ; t > 0; t-- {
		var max int

		fmt.Scanln(&n, &k)
		for i := 1; i < n; i++ {
			for j := i + 1; j <= n; j++ {
				if i&j > max && i&j < k {
					max = i & i
				}
			}
		}

		fmt.Println(max)
	}
}
