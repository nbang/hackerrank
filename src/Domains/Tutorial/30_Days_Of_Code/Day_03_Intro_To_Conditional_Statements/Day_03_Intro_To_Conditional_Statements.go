package main

import "fmt"

func main() {
	var n int

	fmt.Scanln(&n)

	if n%2 == 0 && (n <= 6 || n > 20) {
		fmt.Println("Not Weird")
	} else {
		fmt.Println("Weird")
	}
}
