package main

import "fmt"

func main() {
	var n, max, cur int
	fmt.Scanln(&n)

	for ; n > 0; n = n >> 1 {
		if n%2 == 0 {
			cur = 0
		} else {
			cur++
			if cur > max {
				max = cur
			}
		}
	}

	fmt.Println(max)
}
