package main

import "fmt"

func main() {
	var n int
	var name, phone string

	var phonebook = make(map[string]string)
	fmt.Scanln(&n)

	for ; n > 0; n-- {
		fmt.Scanln(&name, &phone)
		phonebook[name] = phone
	}

	var err error
	for _, err = fmt.Scanln(&name); err == nil; {
		phone = phonebook[name]
		if len(phone) > 0 {
			fmt.Printf("%s=%s\n", name, phone)
		} else {
			fmt.Println("Not found")
		}
		_, err = fmt.Scanln(&name)
	}
}
