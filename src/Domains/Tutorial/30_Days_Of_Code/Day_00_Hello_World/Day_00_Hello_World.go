package main

import "fmt"
import "bufio"
import "os"

func main() {
	var r = bufio.NewReader(os.Stdin)
	var str, _ = r.ReadString('\n')

	fmt.Printf("Hello, World.\n%s", str)
}
