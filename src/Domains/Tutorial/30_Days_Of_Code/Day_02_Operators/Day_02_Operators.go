package main

import "fmt"

func main() {
	var cost float64
	var tip, tax int

	fmt.Scanln(&cost)
	fmt.Scanln(&tip)
	fmt.Scanln(&tax)

	fmt.Printf("The total meal cost is %d dollars.", int(cost/100*float64(100+tip+tax)+0.5))
}
